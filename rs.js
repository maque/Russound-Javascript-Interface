ChannelChanged=false;

function adaptTooltipPadding(attr, number) {

	//addapt padding for one or two digits
	if (number >= 10 || number < 0) {
		$("#"+attr+"_tooltip").css("padding-left", "1px");
	}
	else {
		$("#"+attr+"_tooltip").css("padding-left", "6px");
	}
}

function setVolume() {
	Z=$( "#zone" ).val();
	ZoneConfig[Z].volume = $( "#volume" ).slider( "value" );
	callURL("action=volume&zone=" + Z + "&volume=" + ZoneConfig[Z].volume);
}

function SetVolumeUpDown(value) {
	Z=$( "#zone" ).val();
	vol=$( "#volume" ).slider( "value" ) + value;
	if (vol > 0 && vol <= 50) {
		ZoneConfig[Z].volume = vol;
		callURL("action=volume&zone=" + Z + "&volume=" + vol);
		$( "#volume" ).slider( "value", vol );
		$( "#volume_tooltip").text(vol);

		adaptTooltipPadding("volume", vol); 
	}
}

function SetTurnOnVolume() {
	Z=$( "#zone" ).val();
	vol = $( "#turnonvolume" ).slider( "value" );
	ZoneConfig[Z].turnOnVolume = vol;
	callURL("action=turnOnVolume&zone=" + Z + "&volume=" + ZoneConfig[Z].turnOnVolume);
	ZoneConfig[Z].volume = vol;
	$( "#volume" ).slider( "value", vol );
	$( "#volume_tooltip").text(vol);
	//addapt padding for one or two digits
	adaptTooltipPadding("volume", vol); 
}

function setBass() {
	Z=$( "#zone" ).val();
	ZoneConfig[Z].bass = $( "#bass" ).slider( "value" );
	callURL("action=bass&zone=" + Z + "&bass=" + ZoneConfig[Z].bass);
}

function setBalance() {
	Z=$( "#zone" ).val();
	ZoneConfig[Z].balance = $( "#balance" ).slider( "value" );
	callURL("action=balance&zone=" + Z + "&balance=" + ZoneConfig[Z].balance);
}

function setTreble() {
	Z=$( "#zone" ).val();
	ZoneConfig[Z].treble = $( "#treble" ).slider( "value" );
	callURL("action=treble&zone=" + Z + "&treble=" + ZoneConfig[Z].treble);
}

function Display_Audio_Settings(yesno) {
		
		if (yesno) {
			createCookie('AudioSettings', 'show', 1);

//			window.resizeTo(290,580);

			$( "#turnonvolume" ).show();
			$( "#turnonvolumeminus" ).show();
			$( "#turnonvolumeplus" ).show();
			$( "#turnonvolumetext" ).show();
			$( "#bass" ).show();
			$( "#basstext" ).show();
			$( "#balance" ).show();
			$( "#balancetext" ).show();
			$( "#treble" ).show();
			$( "#trebletext" ).show();

		}
		else {
			createCookie('AudioSettings', 'hide', 1);
//			window.resizeTo(290,388);

			$( "#turnonvolume" ).hide();
			$( "#turnonvolumetext" ).hide();
			$( "#turnonvolumeminus" ).hide();
			$( "#turnonvolumeplus" ).hide();
			$( "#bass" ).hide();
			$( "#basstext" ).hide();
			$( "#balance" ).hide();
			$( "#balancetext" ).hide();
			$( "#treble" ).hide();
			$( "#trebletext" ).hide();
		}
}

function tooltip(sliderObj, ui, attr) {
	if (ui)
		number = ui.value;
	else
		number = sliderObj.slider( "value" );
	
	val = '<div id="'+attr+'_tooltip">'+ number +'</div>';
	
	sliderObj.children('.ui-slider-handle').first().html(val);

	adaptTooltipPadding(attr, number); 
}

function init_Zone() {
	
	Z=$( "#zone" ).val();
	S=$( "#source" ).val();

	if(ZoneConfig[Z].status == "ON"){
		$( "#zoneOnOff" ).data('toggles').toggle(true, true, true);
		$( "#volume" ).slider({ disabled: false });
		$( "#bass" ).slider({ disabled: false });
		$( "#balance" ).slider({ disabled: false });
		$( "#treble" ).slider({ disabled: false });
		$( "#volumeplus").prop('disabled', false);
		$( "#volumeminus").prop('disabled', false);


		vol=ZoneConfig[Z].volume;
	}
	else {
		$( "#zoneOnOff" ).data('toggles').toggle(false, true, true);
		$( "#volume" ).slider({ disabled: true });
		$( "#bass" ).slider({ disabled: true });
		$( "#balance" ).slider({ disabled: true });
		$( "#treble" ).slider({ disabled: true });
		$( "#volumeplus").prop('disabled', true);
		$( "#volumeminus").prop('disabled', true);

		vol=ZoneConfig[Z].turnOnVolume;
	}	

	console.log("INIT_ZONE: Zone selected: " + Z + " - " + ZoneConfig[Z].name + " Volume = " + vol + " Status: "  +
		ZoneConfig[Z].status + " Source : " + SourceConfig[S].name);
	
	createCookie('Zone', Z, 100);

	// change the state without triggering an event
	// onoffToggle.toggle(state, noAnimate, noEvent)

 
	// Set Slider to Zone Volume
	tooltip($( "#volume" ), null, "volume");
	$( "#volume" ).slider( "value", vol );
	$( "#volume_tooltip").text(vol);
	adaptTooltipPadding("volume", vol); 


	$( "#turnonvolume" ).slider( "value", ZoneConfig[$( "#zone" ).val()].turnOnVolume );
	$( "#turnonvolume_tooltip").text(ZoneConfig[$( "#zone" ).val()].turnOnVolume);                  
	$( "#bass" ).slider( "value", ZoneConfig[$( "#zone" ).val()].bass );
	$( "#balance" ).slider( "value", ZoneConfig[$( "#zone" ).val()].balance );
	$( "#treble" ).slider( "value", ZoneConfig[$( "#zone" ).val()].treble );
	

	tooltip($( "#bass" ), null, "bass");
	tooltip($( "#balance" ), null, "balance");
	tooltip($( "#treble" ), null, "treble");

	// Init Source
	$( "#source" ).val(ZoneConfig[Z].currentSource);
	$( "#source" ).selectmenu("refresh");
	init_Source();

}

function select_Zone() {
	init_Zone();
}

function init_Source () {
	Z=$( "#zone" ).val();
	S=$( "#source" ).val();
	SourceName=SourceConfig[S].name;

	if (SourceName.substr(0,5) === 'Radio') {
		// Show Channel, set Channel of selected Source
		$( "#channelselect" ).show();
		$( "#channel" ).val(SourceConfig[S].channel);
		$( "#channel" ).selectmenu("refresh");
	}
	else {
		$( "#channelselect" ).hide();
	}
	ChannelChanged=false;
}

function select_Source() {
		
	Z=$( "#zone" ).val();
	S=$( "#source" ).val();

	SourceName=SourceConfig[S].name;
	
	if(ZoneConfig[Z].status == "ON") {
		//Change Source of current Zone
		callURL("action=source&zone=" + Z + "&source=" + S);
	
		// set currentSource of selected Zone
		ZoneConfig[Z].currentSource = S;
	}
	init_Source();	
}

function select_Channel() {
	Z=$( "#zone" ).val();
	S=$( "#source" ).val();
	C=$( "#channel" ).val();
	
	if(ZoneConfig[Z].status == "ON") {
		// Uodate Channel and change Channel in SourceConfig
		callURL("action=play&zone=" + Z + "&source=" + S + "&channel=" + Channels[C]);
		SourceConfig[S].channel=C;
		ChannelChanged=false;
	}	
	else {
		// Remind me, Channel has been changed
		ChannelChanged=true;
	}	
}

function callURL (urlstr) {
	var xmlhttp;

	if (window.XMLHttpRequest)
		xmlhttp = new XMLHttpRequest();
	else
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            // Do something with the result, like post a notification
      //  $('#notice').html('<p class="success">'+xmlhttp.responseText+'</p>');
		}
	}

	path="/cmd?";
	
	if (EnableSSL == 1 && window.location.protocol == "https:") {
		if (host == "" )
			url = "https://" + window.location.hostname  + ":" + sslport + path + urlstr
		else
			url = "https://" + host  + ":" + sslport + path + urlstr
	}
	else {
		if (host == "" )
			url = "http://" + window.location.hostname  + ":" + port + path + urlstr
		else
			url = "http://" + host  + ":" + port + path + urlstr
	
	}
	console.log(url);
		
	xmlhttp.open('GET',url, true);
	xmlhttp.send();
}


$(function() {

	$.widget("custom.iconselectmenu", $.ui.selectmenu, {
		change: select_Zone,
		_renderItem:
			function (ul, item) {

				var li = $("<li>", {
					text: item.label
				});

				if (item.disabled) {
					li.addClass("ui-state-disabled");
				}
				
				$("<span>", {
					style: item.element.attr("data-style"),
					"class": "ui-icon " + item.element.attr("data-class")
				})
				.appendTo(li);
				return li.appendTo(ul);
			}
	});
	
	$("#zone").iconselectmenu().iconselectmenu("menuWidget").addClass("ui-menu-icons customicons");

		
	$('#zone').iconselectmenu({
		change: select_Zone
	});

	$( "#source" ).selectmenu({
		change: select_Source
	});
	
	$( "#channel" ).selectmenu({
		change: select_Channel
	});


	$( "#volume").slider({
		orientation: "horizontal",
		range: "min",
		min: 1,
		step:1,
		max: 50,
		value: ZoneConfig[$( "#zone" ).val()].volume,
		slide: function( e, ui ) {
			tooltip($(this),ui, "volume");                    
		},              
		create:function(e,ui){
			tooltip($(this),ui, "volume");  
		},
		stop: setVolume
	});
	
	$( "#turnonvolume").slider({
		orientation: "horizontal",
		range: "min",
		min: 1,
		step:1,
		max: 50,
		value: ZoneConfig[$( "#zone" ).val()].turnOnVolume,
		slide: function( e, ui ) {
			tooltip($(this),ui, "turnonvolume");                    
		},              
		create:function(e,ui){
			tooltip($(this),ui, "turnonvolume");  
		},
		stop: SetTurnOnVolume
	});
	
	$( "#bass").slider({
		orientation: "horizontal",
		range: "min",
		min: -10,
		step:1,
		max: 10,
		value: ZoneConfig[$( "#zone" ).val()].bass,
		slide: function( e, ui ) {
			tooltip($(this),ui, "bass");                    
		},              
		create:function(e,ui){
			tooltip($(this),ui, "bass");  
		},
		stop: setBass
	});
	
	$( "#balance").slider({
		orientation: "horizontal",
		range: "min",
		min: -10,
		step:1,
		max: 10,
		value: ZoneConfig[$( "#zone" ).val()].balance,
		slide: function( e, ui ) {
			tooltip($(this),ui, "balance");                    
		},              
		create:function(e,ui){
			tooltip($(this),ui, "balance");  
		},
		stop: setBalance
	});
	
	$( "#treble").slider({
		orientation: "horizontal",
		range: "min",
		min: -10,
		step:1,
		max: 10,
		value: ZoneConfig[$( "#zone" ).val()].treble,
		slide: function( e, ui ) {
			tooltip($(this),ui, "treble");                    
		},              
		create:function(e,ui){
			tooltip($(this),ui, "treble");  
		},
		stop: setTreble
	});
		
 
	$("#settingsOnOff").toggles({
		drag: true,
		text:{
			off:"SHOW",
			on:"HIDE"
		},

		on: false,
		width: 100,
		height: 25
	});

	$("#zoneOnOff").on('toggle', function(e, active) {
		
		Z=$( "#zone" ).val();
		S=$( "#source" ).val();
		C=$( "#channel" ).val();

		e.preventDefault();

		// Toggle set to ON
		if (active) {
			if (ZoneConfig[Z].status == "OFF")
			{
				Url = "action=on&zone=" + Z + "&source=" + S;
				if (ChannelChanged) {
					Url = Url + "&frequency=" + Channels[C];
					ChannelChanged=false;
				}
				callURL(Url);

				ZoneConfig[Z].status = "ON";
				ZoneConfig[Z].currentSource = S;
				SourceConfig[S].channel = C;
				init_Zone();
				   
			}
			else
			{
				console.log('Toggle is already ON!');
			}
		}
		// Toggle set to OFF
		else {
			if (ZoneConfig[Z].status == "ON")
			{
				callURL("action=off&zone=" + Z);
				ZoneConfig[Z].status = "OFF";
				ZoneConfig[Z].volume = ZoneConfig[Z].turnOnVolume;
				init_Zone();
			}
			else {
				console.log('Toggle is already OFF!');
			}
		}
		
		// Update select icon
		$("#zone"+Z).attr("data-class", ZoneConfig[Z].status);
		$( "#zone" ).iconselectmenu("refresh");
		ChannelChanged=false;
	});

	$("#settingsOnOff").on('toggle', function(e, active) {
		if (active) {
			Display_Audio_Settings(true);
		}
		else {
			Display_Audio_Settings(false);
		}
	});

	if (Zone=readCookie('Zone')){
		console.log("Read Cookie Zone: " + Zone)
		$("#zone").val(Zone);
		$("#zone").iconselectmenu("refresh");
	}
		
	if (readCookie('AudioSettings') =='show') {
		$( "#settingsOnOff" ).data('toggles').toggle(true, true, true);
		Display_Audio_Settings(true);
	}
	else {
		$( "#settingsOnOff" ).data('toggles').toggle(false, true, true);
		Display_Audio_Settings(false);
	}
	
	$("#zoneOnOff").toggles({
		drag: true,
		text:{
			on:"ON",
			off:"OFF"
		},
		on: ZoneConfig[$( "#zone" ).val()].status,
		width: 90,
		height: 25
	});

	init_Zone();

}); 
