<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" http-equiv="refresh">
	<meta name="viewport" content="width=a, width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
	<link rel="shortcut icon" type="image/x-icon" href="rs.png">
	<title>Russound Javascript Interface</title>
	
<!--
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

	<script src="//code.jquery.com/jquery-2.2.4.js""></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
-->

	<link rel="stylesheet" href="./jquery/jquery-ui.css"> 

	<script src="./jquery/jquery-2.2.4.js""></script>
	<script src="./jquery/jquery-ui.js"></script>

	<script src="./jquery/jquery.ui.touch-punch.min.js"></script>

	<link rel="stylesheet" href="rs.css">
	<link rel="stylesheet" type="text/css" media="all" href="./toggles/css/toggles.css">
	<link rel="stylesheet" type="text/css" media="all" href="./toggles/css/themes/toggles-iphone.css">
	<script src="rs.js"></script>
	<script src="cookies.js"></script>
	<script type="text/javascript" src="./toggles/toggles.min.js"></script>

</head>
<body >

<div id="Box" class="ui-widget-content">

<form action="#" style="padding-top: 10px;">
<fieldset>
    <select name="zone" id="zone">
	<?php	
		include "./config.php";
		
		function checkChannels() { //Normalize the view of Channels e.g. 96.20 MHzFM => SWR3
			global $sourceconfig, $channels;

			$freq = array_flip($channels);
			foreach ($sourceconfig as $i => $sc) {
				if (!strncmp($sourceconfig[$i]["name"], "Radio", 5)) {
					$c = $sourceconfig[$i]["channel"];
					if (!array_key_exists($c, $channels)) {
						$res = explode(" ", $c);

						$sourceconfig[$i]["channel"] = $freq[$res[0]];
						if (array_key_exists($res[0], $freq)) {
							$sourceconfig[$i]["channel"] = $freq[$res[0]];
						}
						else
							$channels[$res[0]]=$res[0];
					}
				}
			}
			ksort($channels, SORT_STRING | SORT_FLAG_CASE);			
		}

		function sortZone($config, $key) {
			foreach ($config as $z => $val)
				$sort[$z] = $val[$key];
			asort($sort);
			return $sort;
		}
		try {
			if ( empty($host)){
				$host = $_SERVER['HTTP_HOST'];
			}
			if(  $EnableSSL==1  && ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ) {
			//enable secure connection
				$prot="https://";
				$p=$sslport;
			}
			else {
				$prot="http://";
				$p=$port;
			}
				
			$json = file_get_contents($prot . $host . ":" . $p);
			$obj = json_decode($json, true);
			$zoneconfig=$obj["ZoneConfig"][1];
			$sourceconfig=$obj["SourceConfig"];
			$defchannel=$obj["DefaultChannel"];
			$channels=$obj["Channels"];
			$error=0;
			
			foreach (sortZone($zoneconfig, "name") as $z => $val)
				echo '<option value="' . $z . '" id="zone' . $z . '" data-class="' . $zoneconfig[$z]["status"] . '">' . $zoneconfig[$z]["name"] . '</option>';
		}
		catch (exception $e) {
			$error="No Stream!";
		}
	?>
	
    </select>
  </fieldset>
 
</form>


<script>
var ZoneConfig = <?php echo json_encode($zoneconfig, JSON_PRETTY_PRINT); ?>;
var SourceConfig = <?php checkChannels(); echo json_encode($sourceconfig, JSON_PRETTY_PRINT); ?>;
var Channels = <?php echo json_encode($channels, JSON_PRETTY_PRINT); ?>;

var host="<?php echo $host; ?>";
var port="<?php echo $port; ?>";
var sslport="<?php echo $sslport; ?>";
var EnableSSL="<?php echo $EnableSSL; ?>";
var path="<?php echo $path; ?>";


</script>

<form id="onoffForm" method="post" action="#" class="toggle-iphone">
 	<div id="zoneOnOff" class="toggle floatright"></div>
</form>

<div id=sourceselect>
<form action="#">
<fieldset>
	<select name="source" id="source">
		<?php
			ksort($sourceconfig);
			foreach ($sourceconfig as $i => $s)
				echo '<option value="' . $i . '">' . $s["name"]  . '</option>';
		?>
	</select>
</fieldset>
</form>
</div>

<div id=channelselect>
<form action="#">
<fieldset>
	<select name="channel" id="channel">
		<?php
			foreach ($channels as $c => $f)   {
				echo '<option value="' . $c . '">' . $c . '</option>';
			}
		?>
	</select>
</fieldset>
 </form>
</div>
<div style="display: flex; padding-right: 12px; margin-left: 2px; padding-left: 12px; align-items: center;">
	<div id="volume"></div>
	<input type="button" value="-" class="btn" id="volumeminus" style="margin-left: 12px;" onclick="SetVolumeUpDown(-1)">
	<input type="button" value="+" class="btn" id="volumeplus" style="margin-left: 2px;" onclick="SetVolumeUpDown(+1)">
</div>


<div id="volumetext" style="margin-top: -10px;">Volume</div>

<form id="audioForm" method="post" action="#" class="toggle-iphone">
 	<div id="settingsOnOff" class="toggle floatright"></div>
</form>

<div style="display: flex; align-items: center;">
	<div id="turnonvolume"></div>
</div>

<div id="turnonvolumetext">TurnOnVolume</div>
<div id="bass"></div>
<div id="basstext">Bass</div>
<div id="balance"></div>
<div id="balancetext">Balance</div>
<div id="treble"></div>
<div id="trebletext">Treble</div>

</div>

</body>
</html>
