# Russound-Javascript-Interface

Simple JS user interface for MCA-C3, MAC-C5 and MCA-88. Ideally it should be installed on the same server as https://github.com/maaque/Russound-RIO-daemon
It requires a webserver with php support. The settings can be adapted in config.php, how to reached the daemon. Just copy the files as is. In case the url is called with https enabled, it also tries to connect to the RIO-daemon via https:

The connection parameter to Russound-RIO-daemon have to be defined in config.inc.php.

Credits to http://touchpunch.furf.com/, as it enable draging on mobile devices
